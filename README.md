This is an app for keeping contracts and specifications.

Working principle:
Filling out the company card --> 
Entering the contract --> 
Entering the specification --> 
Downloading templates with filled data.

Installation:
mkdir project --> 
cd project --> 
git clone https://gitlab.com/hennadii.honcharuk/vekdoc.git --> 
python3 -m venv folder --> 
source folder/bin/activate --> 
cd vekdoc --> 
pip install -r requirements.txt --> 
python3 manage.py collectstatic --> 
python3 manage.py runserver --> 
go to the link (testuser/7wordpass).

Legend:
Fa pencil - editing;
Fa-bars - related specifications;
fa-caret-down - download contract/specification;
fa-garbage - removal.
