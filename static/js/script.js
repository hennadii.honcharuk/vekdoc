$(document).ready(function(){
  $("#id_agent").click(function(){
    if ($("#id_agent option:selected").text() == "Ні") {
      $("#id_agentrate").attr("disabled", true);}
    else if ($("#id_agent option:selected").text() == "Так") {
      $("#id_agentrate").attr("disabled", false);}
  });
  $("#contragent").autocomplete({
    source: "/get_contragent",
    minLength: 2,
    select: function(event, ui){
      $("#id_contragent").val(ui.item.idn), $("#edrpou").val(ui.item.edrpou);}
  });
  $("#id_contrname").autocomplete({
    source: "/get_contragent",
    minLength: 2,
  });
  $("#transport").autocomplete({
    source: "/get_transport",
    minLength: 0,
    select: function(event, ui){
      $("#transportid").val(ui.item.idn);}
  });
  $("#trade").autocomplete({
    source: "/get_agreement",
    minLength: 2,
    select: function(event, ui){
      $("#id_trade").val(ui.item.idn);}
  });
  $("#auclaim").autocomplete({
    source: "/get_claim",
    minLength: 2,
    select: function(event, ui){
      $("#id_claim").val(ui.item.idn);}
  });
  $('#id_cost').keyup(function(){
    var num=$('#id_cost').val();
    if ($("#id_procent option:selected").text() == "14%") {
      $('#id_notpdv').val(((num*50)/57).toFixed(2));
      $('#id_pdv').val(($('#id_cost').val()-$('#id_notpdv').val()).toFixed(2));}
    else if ($("#id_procent option:selected").text() == "20%") {
      $('#id_notpdv').val(((num*5)/6).toFixed(2));
      $('#id_pdv').val(($('#id_cost').val()-$('#id_notpdv').val()).toFixed(2));}
  });
  $('#id_quantity').keyup(function(){
    var num=(($('#id_quantity').val()*$('#id_notpdv').val()).toFixed(2));
    if ($("#id_procent option:selected").text() == "14%") {
      $('#id_sumnotpdv').val(num);
      var sumcost=parseFloat(num*0.14)+parseFloat(num)
      $('#id_sumpdv').val((sumcost-$('#id_sumnotpdv').val()).toFixed(2));
      $('#id_sumcost').val((sumcost).toFixed(2));}
    else if ($("#id_procent option:selected").text() == "20%") {
      $('#id_sumnotpdv').val(num);
      var sumcost=parseFloat(num*0.2)+parseFloat(num)
      $('#id_sumpdv').val((sumcost-$('#id_sumnotpdv').val()).toFixed(2));
      $('#id_sumcost').val((sumcost).toFixed(2));}
  });
});
