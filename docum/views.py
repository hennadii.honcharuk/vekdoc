import xlwt, csv, io, os
import jinja2
import locale
from io import BytesIO
from num2words import num2words
from django.db import IntegrityError
from django.urls import reverse_lazy, reverse
from datetime import datetime, date, time, timedelta
from vekdoc.settings import BASE_DIR
from django.shortcuts import render, redirect
from django.db.models import Q, F, Sum
from django.db.models.functions import Cast, Coalesce
from docxtpl import DocxTemplate
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.http import JsonResponse, HttpResponse, FileResponse
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Contragent, Product, Contract, Addition
from .forms import ContractForm, AdditionForm
# Create your views here.



def multiple(str, value):
    for i, j in value.items():
        str = str.replace(i, j)
    return str


def toFixed(value, digits=2):
    return f'{value:.{digits}f}'


def outnumber(value):
    list = {'.': ','}
    strnumber = str(toFixed(value))
    number = strnumber[-12:-9]+' '+strnumber[-9:-6]+' '+strnumber[-6:]
    out = multiple(number, list)
    return out


def first(value):
   return value.title()


def convert_date(s):
    s = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + int(s) - 2).strftime('%Y-%m-%d')
    return s


def nword(value):
    list = {'одна кома': 'одна гривня', 'двi кома': 'дві гривні', 'три кома': 'три гривні',
        'чотири кома': 'чотири гривні',  'кома': 'гривень', 'одна копійок': 'одна копійка',
        'двi копійок': 'дві копійки', 'три копійок': 'три копійки', 'чотири копійок': 'чотири копійки'}
    strnumber = str(toFixed(value))
    number = num2words(strnumber, lang='uk')
    fnumber = number+' копійок'
    numword = multiple(fnumber, list)
    return numword



def get_contragent(request):
    if 'term' in request.GET:
        qs = Contragent.objects.filter(longname__icontains=request.GET.get('term'))
        results = list()
        for contragent in qs:
            contragent_json = {'label': contragent.shortname, 'value': contragent.shortname}
            contragent_json['idn'] = contragent.id
            contragent_json['edrpou'] = contragent.edrpou
            results.append(contragent_json)
        return JsonResponse(results, safe=False)



class ContragentCreate(CreateView):
    model = Contragent
    fields = '__all__'
    success_url = reverse_lazy('contragent_list')


class ContragentUpdate(UpdateView):
    model = Contragent
    fields = '__all__'
    success_url = reverse_lazy('contragent_list')


class ContragentList(ListView):
    model = Contragent
    paginate_by = 25

    def get_queryset(self, *args, **kwargs):
        query_name = self.request.GET.get('name')
        if query_name:
            queryset = super().get_queryset().filter(
            Q(longname__icontains=query_name) | Q(edrpou__icontains=query_name))
        else:
            queryset = super().get_queryset().all()
        return queryset



class ContractCreate(CreateView):
    form_class = ContractForm
    template_name = 'docum/contract_create.html'


class ContractUpdate(UpdateView):
    model = Contract
    form_class = ContractForm
    template_name = 'docum/contract_form.html'


class ContractList(ListView):
    model = Contract
    paginate_by = 25

    def get_queryset(self, *args, **kwargs):
        query_name = self.request.GET.get('name')
        if query_name:
            queryset = super().get_queryset().filter(
            Q(number__icontains=query_name) | Q(contragent__longname__icontains=query_name))
        else:
            queryset = super().get_queryset().all()
        return queryset


class ContractDetail(DetailView):
    model = Contract


def contract_download(request, pk):
    obj = Contract.objects.get(pk=pk)
    filename = obj.file_contract.path
    response = FileResponse(open(filename, 'rb'))
    return response


def addition_create(request):
    if request.method == "POST":
        getting = request.POST.get('contract')
        contract = Contract.objects.get(id=getting)
#        if Addition.objects.filter(contract=contract).count() == 0:
#            number = 1
#        else:
#            add = Addition.objects.filter(contract=contract).first()
#            number = int(add.number) + 1
#        addition = Addition(contract=contract, number=number)
        addition = Addition(contract=contract)
        addition.save()
        return redirect('addition_update', pk=addition.pk)


def addition_claim(request):
    if request.method == "POST":
        contragent = Contragent.objects.get(shortname=request.POST.get('contrname'))
        contract = Contract.objects.get(contragent=contragent.id)
        claim = Claim.objects.get(id=request.POST.get('claim'))
        if Addition.objects.filter(contract=contract).count() == 0:
            number = 1
        else:
            add = Addition.objects.filter(contract=contract).first()
            number = int(add.number) + 1
        addition = Addition(contract=contract, number=number, claim=claim)
        addition.save()
        return redirect('addition_update', pk=addition.pk)


class AdditionUpdate(UpdateView):
    model = Addition
    form_class = AdditionForm

    def get_success_url(self):
        getting = self.request.POST.get('contract')
        return reverse('contract_detail', kwargs={'pk': getting})


class AdditionDetail(DetailView):
    model = Addition

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['loading'] = Transport.objects.filter(addition=self.object.id).aggregate(Sum('loading'))['loading__sum']
        context['discharging'] = Transport.objects.filter(addition=self.object.id).aggregate(Sum('discharging'))['discharging__sum']
        return context


class AdditionDelete(DeleteView):
    model = Addition
    success_url = reverse_lazy('contract_list')


def addition_download(request, pk):
    obj = Addition.objects.get(pk=pk)
    filename = obj.file_addition.path
    response = FileResponse(open(filename, 'rb'))
    return response


def create_doc(request, pk):
    contract = Contract.objects.get(pk=pk)
    list = {
        '/': '_', '-': '_', 'директор': 'директора', 'голова': 'голови', 'голова правління': 'голови правління', 'July': 'липня',
        'August': 'серпня', 'September': 'вересня', 'October': 'жовтня', 'November': 'листопада', 'December': 'грудня',
        'January': 'січня', 'February': 'лютого', 'March': 'березня', 'April': 'квітня', 'May': 'травня', 'June': 'червня'}
    list2 = {'/': '_', '-': '_', ' ': '_', '"': '_', '«': '_', '»': '_'}
    date = datetime.strftime(contract.date, '%d %B %Y')
    context = {
        'contract': contract.number, 'date': multiple(date, list), 'cash': contract.cash,
        'sname': contract.contragent.shortname,'cname': contract.contragent.case_leader,
        'phone': contract.contragent.phone, 'email': contract.contragent.email,
        'address': contract.contragent.address, 'iban': contract.contragent.iban,
        'bank': contract.contragent.bank, 'edrpou': contract.contragent.edrpou,
        'ipn': contract.contragent.ipn,'position1': contract.contragent.position,
        'position2': multiple(contract.contragent.position, list), 'leader': contract.contragent.leader}
    byte_io = BytesIO()
    jinja_env = jinja2.Environment()
    jinja_env.filters['first'] = first
    tpl = DocxTemplate(os.path.join(BASE_DIR, 'templates/tempd.docx'))
    tpl.render(context, jinja_env)
    tpl.save(byte_io)
    byte_io.seek(0)
    return FileResponse(byte_io, as_attachment=True, filename='договор_%s_%s.docx' % (multiple(contract.number, list),
        multiple(contract.contragent.shortname, list)))


def create_spec(request, pk):
    addition = Addition.objects.get(pk=pk)
    list = {
        '/': '_', '-': '_', 'директор': 'директора', 'голова': 'голови', 'голова правління': 'голови правління', 'July': 'липня',
        'August': 'серпня', 'September': 'вересня', 'October': 'жовтня', 'November': 'листопада', 'December': 'грудня',
        'January': 'січня', 'February': 'лютого', 'March': 'березня', 'April': 'квітня', 'May': 'травня', 'June': 'червня'}
    list2 = {'/': '_', '-': '_', ' ': '_', '"': '_', '«': '_', '»': '_'}
    delta = addition.date + timedelta(10)
    date = datetime.strftime(addition.date, '%d %B %Y')
    date2 = datetime.strftime(addition.date, '%d.%m.%Y')
    date3 = datetime.strftime(delta, '%d %B %Y')
    contract_date = datetime.strftime(addition.contract.date, '%d.%m.%Y')
    if addition.tencent == False:
        tencent = ' тонн.'
    else:
        tencent = ' тонн  +/-10%.'
    context = {
        'number': addition.number, 'date': multiple(date, list), 'date2': date2, 'date3': multiple(date3, list),
        'contract': addition.contract.number, 'contract_date': contract_date, 'tencent': tencent,
        'sname': addition.contract.contragent.shortname, 'lnameproduct': addition.product.longname,
        'year': addition.product.period, 'zed': addition.product.utkzed, 'dstu': addition.product.dstu,
        'condition': addition.product.condition, 'quantity': outnumber(addition.quantity),
        'cost': outnumber(addition.cost), 'pdv': outnumber(addition.pdv), 'procent': addition.procent,
        'notpdv': outnumber(addition.notpdv), 'sumcost': outnumber(addition.sumcost),
        'sumpdv': outnumber(addition.sumpdv), 'sumnotpdv': outnumber(addition.sumnotpdv),
        'trade': addition.trade, 'numword': nword(addition.sumcost), 'addresstrade': addition.addresstrade,
        'cname': addition.contract.contragent.case_leader, 'phone': addition.contract.contragent.phone,
        'email': addition.contract.contragent.email, 'address': addition.contract.contragent.address,
        'iban': addition.contract.contragent.iban, 'bank': addition.contract.contragent.bank,
        'edrpou': addition.contract.contragent.edrpou, 'ipn': addition.contract.contragent.ipn,
        'position1': addition.contract.contragent.position, 'leader': addition.contract.contragent.leader,
        'position2': multiple(addition.contract.contragent.position, list)}
    byte_io = BytesIO()
    jinja_env = jinja2.Environment()
    jinja_env.filters['first'] = first
    tpl = DocxTemplate(os.path.join(BASE_DIR, 'templates/temps.docx'))
    tpl.render(context, jinja_env)
    tpl.save(byte_io)
    byte_io.seek(0)
    return FileResponse(
        byte_io, as_attachment=True, filename='спец_%s_%s_%s.docx' % (addition.number, multiple(addition.contract.number, list),
            multiple(addition.contract.contragent.shortname, list)))


def export_contract(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="contract.xls"'
    rows = Contract.objects.all()
    wb = xlwt.Workbook(encoding='utf-8')
    sheet = wb.add_sheet('Дог')
    # Определяем формат ячеек для дат
    date_style = xlwt.XFStyle()
    date_style.num_format_str = "DD.MM.YYYY"
    # Определяем формат ячеек для чисел
    num_style = xlwt.XFStyle()
    num_style.num_format_str = "0"
    # Определяем ширину колонок
    sheet.col(0).width = 4000
    sheet.col(1).width = 8000
    sheet.col(2).width = 4000
    sheet.col(3).width = 4000
    sheet.col(4).width = 8000
    # Добавляем шапку таблицы
    sheet.write(0, 0, u"ЄДРПОУ")
    sheet.write(0, 1, u"Коротка назва")
    sheet.write(0, 2, u"Номер договору")
    sheet.write(0, 3, u"Дата договору")
    sheet.write(0, 4, u"Назва файлу")
    # Обход показаний, statements — объект QuerySet из Джанго
    for i, s in enumerate(rows):
        # i — индекс итерации, используется как номер строки
        sheet.write(i+1, 0, s.contragent.edrpou)
        sheet.write(i+1, 1, s.contragent.shortname)
        sheet.write(i+1, 2, s.number)
        sheet.write(i+1, 3, s.date, date_style)
        sheet.write(i+1, 4, s.file_contract.name)
    # Запись файла
    wb.save(response)
    return response


def export_addition(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="addition.xls"'
    rows = Addition.objects.all()
    wb = xlwt.Workbook(encoding='utf-8')
    sheet = wb.add_sheet('Спец')
    # Определяем формат ячеек для дат
    date_style = xlwt.XFStyle()
    date_style.num_format_str = "DD.MM.YYYY"
    # Определяем формат ячеек для чисел
    num_style = xlwt.XFStyle()
    num_style.num_format_str = "0"
    # Определяем ширину колонок
    sheet.col(0).width = 4000
    sheet.col(1).width = 8000
    sheet.col(2).width = 8000
    sheet.col(3).width = 4000
    sheet.col(4).width = 3000
    sheet.col(5).width = 4000
    sheet.col(6).width = 4000
    sheet.col(7).width = 4000
    sheet.col(8).width = 4000
    # Добавляем шапку таблицы
    sheet.write(0, 0, u"ЄДРПОУ")
    sheet.write(0, 1, u"Коротка назва")
    sheet.write(0, 2, u"Адреса поставки")
    sheet.write(0, 3, u"Номер договору")
    sheet.write(0, 4, u"Номер спец-ї")
    sheet.write(0, 5, u"Дата спец-ї")
    sheet.write(0, 6, u"Культура")
    sheet.write(0, 7, u"Об'єм")
    sheet.write(0, 8, u"Ціна за тонну")
    # Обход показаний, statements — объект QuerySet из Джанго
    for i, s in enumerate(rows):
        # i — индекс итерации, используется как номер строки
        sheet.write(i+1, 0, s.contract.contragent.edrpou)
        sheet.write(i+1, 1, s.contract.contragent.shortname)
        sheet.write(i+1, 2, s.addresstrade)
        sheet.write(i+1, 3, s.contract.number)
        sheet.write(i+1, 4, s.number)
        sheet.write(i+1, 5, s.date, date_style)
        sheet.write(i+1, 6, s.product.shortname)
        sheet.write(i+1, 7, s.quantity)
        sheet.write(i+1, 8, s.cost)
    # Запись файла
    wb.save(response)
    return response


def export_contragent(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="contragent.xls"'
    rows = Contragent.objects.all()
    wb = xlwt.Workbook(encoding='utf-8')
    sheet = wb.add_sheet('Контрагент')
    # Определяем формат ячеек для дат
    date_style = xlwt.XFStyle()
    date_style.num_format_str = "DD.MM.YYYY"
    # Определяем формат ячеек для чисел
    num_style = xlwt.XFStyle()
    num_style.num_format_str = "0"
    # Определяем ширину колонок
    sheet.col(0).width = 4000
    sheet.col(1).width = 4000
    sheet.col(2).width = 8000
    sheet.col(3).width = 4000
    sheet.col(4).width = 4000
    # Добавляем шапку таблицы
    sheet.write(0, 0, u"ЄДРПОУ")
    sheet.write(0, 1, u"Коротка назва")
    sheet.write(0, 2, u"Адрес")
    sheet.write(0, 3, u"Керівник")
    sheet.write(0, 4, u"Контакт")
    # Обход показаний, statements — объект QuerySet из Джанго
    for i, s in enumerate(rows):
        # i — индекс итерации, используется как номер строки
        sheet.write(i+1, 0, s.edrpou)
        sheet.write(i+1, 1, s.shortname)
        sheet.write(i+1, 2, s.address)
        sheet.write(i+1, 3, s.leader)
        sheet.write(i+1, 4, s.telcontact)
    # Запись файла
    wb.save(response)
    return response