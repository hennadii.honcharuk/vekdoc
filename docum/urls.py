from django.urls import path
from django.contrib.auth import views
from django.views.generic import TemplateView
from .views import (ContragentList, ContragentCreate, ContragentUpdate, get_contragent, addition_claim,
    create_doc, create_spec, ContractList, ContractCreate, ContractUpdate, addition_download,
    ContractDetail, AdditionUpdate, AdditionDelete, AdditionDetail, addition_create, contract_download,
    export_contract, export_addition, export_contragent)
  


urlpatterns = [
    path('', TemplateView.as_view(template_name="docum/startpage.html")),
    path('export/', TemplateView.as_view(template_name="docum/export.html"), name='export'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('get_contragent/', get_contragent, name='get_contragent'),
    path('contragent/', ContragentList.as_view(), name='contragent_list'),
    path('contragent/create/', ContragentCreate.as_view(), name='contragent_create'),
    path('contragent/<int:pk>/update/', ContragentUpdate.as_view(), name='contragent_update'),
    path('claim/addition/', addition_claim, name='addition_claim'),
    path('contract/', ContractList.as_view(), name='contract_list'),
    path('contract/create/', ContractCreate.as_view(), name='contract_create'),
    path('contract/<int:pk>/', ContractDetail.as_view(), name='contract_detail'),
    path('contract/<int:pk>/update/', ContractUpdate.as_view(), name='contract_update'),
    path('contract/<int:pk>/createdoc/', create_doc, name='document_create'),
    path('contract/<int:pk>/download', contract_download, name='contract_download'),
    path('addition/create/', addition_create, name='addition_create'),
    path('addition/<int:pk>/delete/', AdditionDelete.as_view(), name='addition_delete'),
    path('addition/<int:pk>/update/', AdditionUpdate.as_view(), name='addition_update'),
    path('addition/<int:pk>/detail/', AdditionDetail.as_view(), name='addition_detail'),
    path('addition/<int:pk>/createspec/', create_spec, name='spec_create'),
    path('addition/<int:pk>/download', addition_download, name='addition_download'),
    path('export_contract/', export_contract, name='export_contract'),
    path('export_addition/', export_addition, name='export_addition'),
    path('export_contragent/', export_contragent, name='export_contragent'),
]