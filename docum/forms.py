from django import forms
from .models import Contract, Addition



class ContractForm(forms.ModelForm):
    class Meta:
        model = Contract
        fields = ('contragent', 'number', 'date', 'cash', 'file_contract')
        widgets = {
            'contragent': forms.HiddenInput(),
            'date': forms.DateInput(format='%Y-%m-%d', attrs={'type':'date'}),}




class AdditionForm(forms.ModelForm):
    class Meta:
        model = Addition
        fields = ('number', 'date', 'contract', 'product', 'cost', 'procent', 'pdv', 'notpdv', 'quantity',
        'sumcost', 'sumpdv', 'sumnotpdv', 'trade', 'addresstrade', 'file_addition', 'tencent')
        widgets = {
            'contract': forms.HiddenInput(),
            'claim': forms.HiddenInput(),
            'date': forms.DateInput(format='%Y-%m-%d', attrs={'type':'date'}),}