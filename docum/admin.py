from django.contrib import admin
from .models import Contragent, Product, Contract, Addition
from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields
# Register your models here.



class ProductResource(resources.ModelResource):
    class Meta:
        model = Product

class ProductAdmin(ImportExportModelAdmin):
    resource_class = ProductResource

admin.site.register(Product, ProductAdmin)



class ContragentResource(resources.ModelResource):
    class Meta:
        model = Contragent

class ContragentAdmin(ImportExportModelAdmin):
    resource_class = ContragentResource

admin.site.register(Contragent, ContragentAdmin)



class ContractResource(resources.ModelResource):
    class Meta:
        model = Contract

class ContractAdmin(ImportExportModelAdmin):
    resource_class = ContractResource

admin.site.register(Contract, ContractAdmin)



class AdditionResource(resources.ModelResource):
    class Meta:
        model = Addition

class AdditionAdmin(ImportExportModelAdmin):
    resource_class = ContractResource

admin.site.register(Addition, AdditionAdmin)

