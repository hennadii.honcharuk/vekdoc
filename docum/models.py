from django.db import models
from django.urls import reverse
from datetime import datetime, date, timedelta


# Create your models here.


class Contragent(models.Model):
    POSITION_CHOICES = [
        ('директор', 'директор'),
        ('голова', 'голова'),
        ('голова правління', 'голова правління'),]
    shortname = models.CharField(max_length=32)
    longname = models.CharField(max_length=128, unique=True)
    edrpou =  models.CharField(max_length=10)
    ipn =  models.CharField(max_length=12)
    address =  models.CharField(max_length=128)
    iban =  models.CharField(max_length=29)
    bank =  models.CharField(max_length=64)
    position = models.CharField(max_length=16, choices=POSITION_CHOICES, default='директор')
    leader = models.CharField(max_length=64)
    case_leader = models.CharField(max_length=64)
    phone = models.CharField(max_length=15, default='_')
    email = models.EmailField(max_length=64, default='_')
    contact = models.CharField(max_length=64)
    telcontact = models.CharField(max_length=15)

    class Meta:
        ordering = ['edrpou']

    def __str__(self):
        return '%s' % (self.shortname)


class Product(models.Model):
    PERIOD_CHOICES = [
        ('врожаю 2021 року', '2021 року'),
        ('врожаю 2022 року', '2022 року'),]
    shortname = models.CharField(max_length=24)
    longname = models.CharField(max_length=64)
    period = models.CharField(max_length=16, choices=PERIOD_CHOICES, default='2021 року')
    utkzed = models.CharField(max_length=14)
    dstu = models.CharField(max_length=9, blank=True, null=True)
    condition = models.CharField(max_length=640, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return '%s %s' % (self.shortname, self.period)

    class Meta:
        ordering = ['shortname']


class Contract(models.Model):
    CASH_CHOICES = [
            ('Покупець здійснює передоплату у розмірі вартості Товару  без врахування суми ПДВ. Постачальник зобов’язується надати Покупцю нижчезазначені документи:', 'Передоплата'),
            ('Покупець оплачує вартість поставленого Товару без врахування суми ПДВ, протягом 3 (трьох) банківських днів, з дати наступної за днем підписання Сторонами видаткової накладної та отриманням Покупцем нижчезазначених документів:', 'Післяплата'),]
    number = models.CharField(max_length=10, unique=True)
    date = models.DateField(default=date.today)
    contragent = models.ForeignKey('Contragent', on_delete = models.CASCADE)
    cash = models.CharField(max_length=256, choices=CASH_CHOICES, default='Передоплата')
    file_contract = models.FileField(upload_to='contract/', blank=True, null=True)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return '%s %s' % (self.number, self.contragent)

    def get_absolute_url(self):
        return reverse('contract_detail', kwargs={'pk': self.pk})


class Addition(models.Model):
    PROCENT_CHOICES = [
        ('14%', '14%'),
        ('20%', '20%'),]
    TRADE_CHOICES = [
        ('CPT', 'CPT'),
        ('FCA', 'FCA'),
        ('DAP', 'DAP'),]
    number = models.CharField(max_length=10)
    date = models.DateField(default=date.today)
    contract = models.ForeignKey('Contract', on_delete = models.CASCADE)
    product = models.ForeignKey('Product', on_delete = models.CASCADE, blank=True, null=True)
    cost = models.FloatField(blank=True, null=True)
    tencent = models.BooleanField(default=False)
    procent = models.CharField(max_length=3, choices=PROCENT_CHOICES, default='14%')
    pdv = models.FloatField(blank=True, null=True)
    notpdv = models.FloatField(blank=True, null=True)
    quantity = models.FloatField(blank=True, null=True)
    sumcost = models.FloatField(blank=True, null=True)
    sumpdv = models.FloatField(blank=True, null=True)
    sumnotpdv = models.FloatField(blank=True, null=True)
    trade = models.CharField(max_length=16, choices=TRADE_CHOICES, default='FCA')
    addresstrade =models.CharField(max_length=200, blank=True, null=True)
    file_addition = models.FileField(upload_to='addition/', blank=True, null=True)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return '№%s %s %s' % (self.number, self.contract, self.product)